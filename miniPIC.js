/**
 *  JavaScript PIC Simulation for Plasma Wakefield
 * ================================================
 *  By Veronica Berglyd Olsen (2015)
 *  Based on code from http://www.playfuljs.com/particle-effects-are-easy
 */

/**
 * Simulation Varianles
 */

// Grid
var boxX  = 1000; // Simulation box width in pixels
var boxY  = 240;  // Simulation box height in pixels
var gridX = 40;   // Grid size in x. boxX/gridX must be integer
var gridY = 40;   // Grid size in y. boxY/gridY must be integer

// Plasma
var nPX   = 2;    // Plasma particles per cell in x
var nPY   = 2;    // Plasma particles per cell in y
var dPT   = 0.1;  // Plasma temperature
var dPQ   = -1.0; // Plasma particle charge

// Calculate additional varianles
var nX = boxX/gridX;      // Number of cells in x
var nY = boxY/gridY;      // Number of cells in y
var nP = nX*nPX * nY*nPY; // Number of plasma particles



console.log(boxX, boxY, gridX, gridY, nX, nY, nP);

// Set up simulation environment
var display = document.getElementById('display');
var rect = display.getBoundingClientRect();
var ctx = display.getContext('2d');
var width = display.width = boxX;
var height = display.height = boxY;
var mouse = { x: width * 0.5, y: height * 0.5 };

/**
 * Additional Math Functions
 */

// Box-Muller transformation
function fRandomGauss() {

    do {
        var x1 = 2.0*Math.random() - 1.0;
        var x2 = 2.0*Math.random() - 1.0;
        var w  = x1*x1 + x2*x2;
    } while(w >= 1.0)

    w = Math.sqrt((-2.0*Math.log(w))/w);

    return [x1*w, x2*w];
}

// Generate HTML color
function fColor(r,g,b) {
    var num = r*256*256 + g*256 + b;
    var col = num.toString(16);
    return "#000000".substring(0,7-col.length)+col;
}

function fCharge2Grid(species) {
    for(var i=0; i<species.length; i++) {

        gX  = species[i].x/gridX - 0.5;
        gY  = species[i].y/gridY - 0.5;
        dX  = (gX+1)%1;
        dY  = (gX+1)%1;

        gXL = (Math.floor(gX)+nX)%nX;
        gYL = (Math.floor(gY)+nY)%nY;
        gXU = (gXL+nX+1)%nX;
        gYU = (gYL+nY+1)%nY;

        grid[gXL][gYL].addcharge(dPQ*(1-dX)*(1-dY));
        grid[gXU][gYL].addcharge(dPQ*   dX *(1-dY));
        grid[gXL][gYU].addcharge(dPQ*(1-dX)*   dY );
        grid[gXU][gYU].addcharge(dPQ*   dX *   dY );
    }
}

function fMouse2Grid(x, y) {

    gX  = x/gridX - 0.5;
    gY  = y/gridY - 0.5;
    dX  = (gX+1)%1;
    dY  = (gX+1)%1;

    gXL = (Math.floor(gX)+nX)%nX;
    gYL = (Math.floor(gY)+nY)%nY;
    gXU = (gXL+nX+1)%nX;
    gYU = (gYL+nY+1)%nY;

    grid[gXL][gYL].addcharge(-10*(1-dX)*(1-dY));
    grid[gXU][gYL].addcharge(-10*   dX *(1-dY));
    grid[gXL][gYU].addcharge(-10*(1-dX)*   dY );
    grid[gXU][gYU].addcharge(-10*   dX *   dY );
}

/**
 * Grid
 */

function Grid(x, y) {
    this.x = x;
    this.y = y;
    this.q = nPX*nPY;
}

Grid.prototype.addcharge = function(q) {
    this.q += q;
}

Grid.prototype.reset = function() {
    this.q = nPX*nPY;
}

Grid.prototype.draw = function() {
    //~ ctx.strokeStyle = '#0000ff';
    //~ ctx.fillStyle   = '#9999ff';
    colMag = Math.abs(Math.floor(this.q*32));
    if(colMag > 255) {colMag = 255;};
    if(this.q < 0.0) {
        ctx.fillStyle = fColor(0,0,colMag);
    } else {
        ctx.fillStyle = fColor(colMag,0,0);
    }
    ctx.beginPath();
    ctx.fillRect(this.x-0.5*gridX, this.y-0.5*gridY, gridX, gridY);
    //~ ctx.stroke();
    //~ ctx.fill();
}

/**
 * Particles
 */

function Particle(x, y, vX, vY, thX, thY, bX, bY, colS, colF) {
    this.x    = this.oldX = x;
    this.y    = this.oldY = y;
    thermal   = fRandomGauss();
    this.vX   = thX*thermal[0] + vX;
    this.vY   = thY*thermal[1] + vY;
    this.bX   = bX;
    this.bY   = bY;
    this.colS = colS;
    this.colF = colF;
}

Particle.prototype.integrate = function() {

    this.oldX = this.x;
    this.oldY = this.y;
    var vX    = this.vX;
    var vY    = this.vY;
    this.x   +=  vX;
    this.y   +=  vY;

    if(this.bX == 0) {
        if(this.x < 0 || this.x > width) {
            this.x -=  vX;
            this.vX = -this.vX;
        }
    } else {
        if(this.x < 0)     this.x += width;
        if(this.x > width) this.x -= width;
    }

    if(this.bY == 0) {
        if(this.y < 0 || this.y > height) {
            this.y -=  vY;
            this.vY = -this.vY;
        }
    } else {
        if(this.y < 0)      this.y += height;
        if(this.y > height) this.y -= height;
    }
}

Particle.prototype.force = function(x, y) {
    //~ var dx = x - this.x;
    //~ var dy = y - this.y;
    //~ var distance = Math.sqrt(dx * dx + dy * dy);
    //~ this.x += dx / distance;
    //~ this.y += dy / distance;
    //~ this.x = this.x;
    //~ this.y = this.y;
}

Particle.prototype.draw = function() {
    //~ ctx.strokeStyle = '#ff0000';
    //~ ctx.fillStyle   = '#ff9999';
    ctx.strokeStyle = this.colS;
    ctx.fillStyle   = this.colF;
    //~ ctx.strokeRect(this.x,this.y,2,2);
    //~ ctx.fillRect(this.x,this.y,3,3);
    //~ ctx.fillText("e",this.x,this.y);
    ctx.beginPath();
    ctx.arc(this.x, this.y, 3, 0, 2*Math.PI);
    ctx.stroke();
    ctx.fill();
}

/**
 * Simulation Set-up
 */

// Create grid

var grid = Array(nX);
for(x=0; x<nX; x++) {
    grid[x] = Array(nY);
    for(y=0; y<nY; y++) {
        grid[x][y] = new Grid(0.5*gridX + x*gridX, 0.5*gridY + y*gridY);
    }
}

// Set up uniform plasma

var plasma = Array(nP);
var deltaX = gridX/nPX;
var deltaY = gridY/nPY;
var iPart  = 0;

for(x=0; x<nX; x++) {
    for(y=0; y<nY; y++) {
        var cellX   = grid[x][y].x;
        var cellY   = grid[x][y].y;
        var originX = cellX - 0.5*gridX;
        var originY = cellY - 0.5*gridY;
        for(m=0; m<nPX; m++) {
            for(n=0; n<nPY; n++) {
                pX = originX + (m+0.5)*deltaX;
                pY = originY + (n+0.5)*deltaY;
                plasma[iPart] = new Particle(pX, pY, 0, 0, dPT, dPT, 0, 0, '#330000', '#ff9999');
                iPart++;
            }
        }
    }
}
fCharge2Grid(plasma);
//~ console.log(plasma[0]);
//~ console.log(grid[0][0]);

// Create Beam

var beam = Array(30);

for(b=0; b<beam.length; b++) {
    dist = fRandomGauss();
    beam[b] = new Particle(gridX + 12*dist[0], 0.5*height+4*dist[1], 1, 0, 0.01, 0.0001, 1, 1, '#003300', '#99dd99');
}
fCharge2Grid(beam);


// Mouse event

display.addEventListener('mousemove', onMousemove);
function onMousemove(e) {
    mouse.x = e.clientX - rect.left;
    mouse.y = e.clientY - rect.top;
    //console.log(mouse.x, mouse.y);
}

/**
 * Main Loop
 */

requestAnimationFrame(frame);
function frame() {
    requestAnimationFrame(frame);
    ctx.clearRect(0, 0, width, height);

    for(x=0; x<nX; x++) {
        for(y=0; y<nY; y++) {
            grid[x][y].reset();
        }
    }

    fCharge2Grid(plasma);
    fCharge2Grid(beam);
    fMouse2Grid(mouse.x, mouse.y);

    for(x=0; x<nX; x++) {
        for(y=0; y<nY; y++) {
            grid[x][y].draw();
        }
    }

    // Draw plasma electrons
    for(var i=0; i<plasma.length; i++) {
        //~ plasma[i].force(mouse.x, mouse.y);
        plasma[i].integrate();
        plasma[i].draw();
    }

    // Draw beam electrons
    for(var i=0; i<beam.length; i++) {
        //~ beam[i].force(mouse.x, mouse.y);
        beam[i].integrate();
        beam[i].draw();
    }

}
